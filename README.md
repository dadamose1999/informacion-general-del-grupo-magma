# MAGMA Ingeniería
Grupo de investigación de la matemática aplicada a la ingeniería adscrito al [Programa de Ingeniería Electrónica](https://www.unimagdalena.edu.co/presentacionPrograma/Programa/1005) de la [Universidad del Magdalena.](https://www.unimagdalena.edu.co/)

<br>

## Datos Básicos:
- **Gran área**: Ingeniería y Tecnología.
- **Categoría**: A1.
- **Creación del grupo**: Febrero, 2002.
- **Área de conocimiento**: Ingeniería eléctrica, electrónica e informática.
- **Código del grupo**: COL0005978.
- **Programa nacional de CyT**: Ciencia, tecnología e innovación en ingeniería.
- **Página web**: [magma.megaingenieria.com](http://magma.megaingenieria.com)
- **GrupLAC**: [MAGMA INGENIERÍA](https://scienti.minciencias.gov.co/gruplac/jsp/visualiza/visualizagr.jsp?nro=00000000002680)
- **Perfiles del grupo - MinCiencias**: [MAGMA INGENIERÍA](https://scienti.minciencias.gov.co/gruplac/jsp/Medicion/graficas/verPerfiles.jsp?id_convocatoria=19&nroIdGrupo=00000000002680)

<br>

## Vinculación al grupo:
- **Modalidad de grado artículo científico:** [Ver infografía.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/blob/main/tesistas/modalidad_de_grado_articulo_cientifico/infografia_articulo_cientifico.pdf)
- **Modalidad de grado trabajo de investigación:** [Ver infografía.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/blob/main/tesistas/modalidad_de_grado_trabajo_de_investigacion/infografia_trabajo_de_investigacion.pdf)
- **Modalidad de grado pasantía de investigación:** [Ver infografía.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/blob/main/pasantes/infografia_pasantia_de_investigacion.pdf)
- **Semillerista:** Contactar a un integrante del grupo de investigación y estar atento a las convocatorias.

<br>

## Información importante para cada rol:
- [Contratistas](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/tree/main/contratistas/)
- [Tesistas (trabajo de investigación y artículo científico)](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/tree/main/tesistas)
- [Pasantes](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/tree/main/pasantes)
- [Semilleristas](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/tree/main/semilleristas)

<br>

## Documentos:
- **Guía de perfil de proyectos de investigación:** [Descargar aquí.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/raw/main/documentos/guia_de_perfil_de_proyectos_de_investigacion.docx?inline=false)
- **Banco de perfiles y convocatorias:** [Descargar aquí.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/raw/main/documentos/banco_perfiles_y_convotarias.xlsx?inline=false)

<br>

## Recursos:
- **Colores óptimos para artículos científicos:** [Ver colores.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/blob/main/recursos/colores_optimos_para_papers.jpeg)
- **Consultar información de revistas científicas:** [Consultar.](https://www.scimagojr.com)
- **Logo del grupo:** [Ver logo.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/raw/main/recursos/magma.jpg)

