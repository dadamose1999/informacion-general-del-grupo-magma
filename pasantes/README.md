# Pasantes de investigación

- **Infografía:** [Ver infografía.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/blob/main/pasantes/infografia_pasantia_de_investigacion.pdf)
- **Formato entrega documentos Consejo de Programa:** [Descargar aquí.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/raw/main/pasantes/formato_entrega_documentos_consejo_de_programa.docx?inline=false)
- **Formato modelo carta aval de la pasantía:** [Descargar aquí.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/raw/main/pasantes/formato_modelo_carta_aval_pasantia_de_investigacion.docx?inline=false)
- **Formato presentación de propuestas:** [Descargar aquí.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/raw/main/pasantes/formato_presentacion_de_propuesta.docx?inline=false)
- **Formato de modificaciones a la propuesta de la pasantía:** [Descargar aquí.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/raw/main/pasantes/formato_de_modificaciones_a_la_propuesta_de_pasantia.docx?inline=false)
- **Formato presentación informe final:** [Descargar aquí.](https://gitlab.com/magma-ingenieria/informacion-general-del-grupo-magma/-/raw/main/pasantes/formato_presentacion_informe_final.docx?inline=false)
